FROM klakegg/hugo

COPY . /src

EXPOSE 1313

CMD ["hugo"]

ENTRYPOINT ["hugo", "server"]
