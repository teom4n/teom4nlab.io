+++
title = 'Earning the First Bucks on Upwork'
date = 2024-05-02T20:00:00+03:00
description = "Getting started on Upwork"
tags = ["upwork", "general"]
+++

Getting started with side gigs can be quite a hassle at first, but I think they just depend on few simple factors. I believe following through them should get you started rather quickly. My first advice is actually lays in the title: focusing on one major platform: Upwork. In the last several years, I've tried multiple platforms, and got the most interaction there. So, I will leave my focus for this article on Upwork only.

## Who am I

I am a 22 years old who do DevOps & Linux administration work on the office and occasionally freelance as well. 

I'm sure there are lots of successful freelancer 16 year olds out there, but when I was 16, things didn't work out as wished them to be on Upwork. That made me focus on my skills rather than keep trying on the market, but I wish I stuck around longer while I was working on. 


## Having the right portfolio

Being clear on the description is the key: what are your daily responsibilities, what have you worked on, tech stack, maybe some future goals and that should be it. This brief and precise description is more than enough. 


## Choosing the right jobs

You should be selective of your jobs to apply for a lot of reasons: you don't want to pay for connects, you actually want to get hired for the project, better the gig better the pay, etc. My criteria is usually: 

 - Less than 5 applicants
 - Well-rated client
 - Payment verified
 - Precise description of the job

The money mostly doesn't matter, you should focus on having satisfied clients. Even if you spend much time than you expected at first, making the client happy will have your profile rated better and they WILL want to work with you again, in the future.

## Proposals

Proposals maybe play the biggest role on getting the job or not. I have friends who are great in what they do but simply don't get much action just because they're not expressing themselves the right way.

Read the job post thoroughly. Understand the objective clearly. I believe the ideal proposal looks something like this (what worked well for me so far):


```Hi!```

```I have worked with X before, and manage Y at my full time job. I believe this should take 2-3 depending on your other environmental factors and my availability. We can have a meet and I can start immediately.```

```Thanks!``` 



## Having a blog & media presence

Well, this is what I currently work on.  I think it'd People want to work with people who share their expertise in various media channels. So putting in the time to create blog posts that show technical skills won't hurt. This also being my first blog post!




